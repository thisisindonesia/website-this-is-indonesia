<?php
	session_start();
	$email = $_SESSION['email'];
	error_reporting(0);
	$conn = mysqli_connect('localhost','root','','thisisindonesia');
	$test = mysqli_query('select * from user');
	if ($test !== true) {
		$conn = mysqli_connect('192.168.1.2:3306','root','pass','thisisindonesia');
	}
	$query = "select nama_depan, nama_belakang, password, tentang_saya, foto_profile FROM user WHERE email = '$email'";
	$hasil = mysqli_query($conn,$query);
	$tampil = mysqli_fetch_array($hasil);
	$depan = $tampil[0];
	$belakang = $tampil[1];
	$password = $tampil[2];
	$tentang = $tampil[3];
	$foto = $tampil[4];
?>
<!DOCTYPE html> 
<html>
	<head>
		<title>Profile</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<link href="open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
	</head>
	<body style="background-image: url(bgupdate.jpg); background-position: center;  background-repeat: no-repeat;  background-size: cover; width: 100%;">
    <nav class="navbar navbar-expand-lg bg-light navbar-light">
      <a class="navbar-brand" href="">
        <img src="logo.png" alt="Logo" style="width: 100px;">
      </a>
      <ul class="nav navbar-nav ml-auto">
			<li class="nav-item">
        <a class="nav-link" href="../Abdullah/home.php">HOME</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="OurCulture.php">OUR CULTURE</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../Andika/Contact.php">ABOUT US</a>
      </li>
      <li class="nav-item">
	  <a href="PostUtama.php"><button type="button" class="btn btn-primary">+ Upload</button></a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
					<img src="../Abdullah/upload/<?=$tampil[4]?>" class="rounded-circle" style="width: 40px; height: 40px;">
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="../Fikri/c.php">My Post</a>
					<a class="dropdown-item" href="#">My Profile</a>
					<a class="dropdown-item" href="../Abdullah/controller/logout.php">Log out</a>					
				</div>
      </ul>
		</nav>
		<br>
		<div class="container-fluid shadow p-3 mb-5 bg-white rounded" style="width:40%;">
			<form action="updateController.php" method="POST" id="update"  enctype="multipart/form-data">
				<h5 style="text-align: center;">Y O U R&nbsp;&nbsp;P R O F I L E</h5>
				<br>
				<img src="../Abdullah/upload/<?=$tampil[4]?>" class="rounded-circle mx-auto d-block" style="width: 125px; height: 125px; text-align: center;">
				<br>
				<br>
				<div class="form-group">
    			<h6 style="text-align: center;">About You</h6>
   			 	<input type="text" class="form-control" id="aboutyou" value="<?=$tampil[3]?>" style="text-align: center;" name="tentang">
				</div>
				<div class="form-group">
    			<h6 style="text-align: center;">First Name</h6>
   			 	<input type="text" class="form-control" id="fname" value="<?=$tampil[0]?>" style="text-align: center;" name="depan">
				</div>
				<div class="form-group">
    			<h6 style="text-align: center;">Last Name</h6>
   			 	<input type="text" class="form-control" id="lname" value="<?=$tampil[1]?>" style="text-align: center;" name="belakang">
				</div>
				<div class="form-group">
    			<h6 style="text-align: center;">Password</h6>
   			 	<input type="text" class="form-control" id="pass" value="<?=$password?>" style="text-align: center;" name="password">
				</div>
				<br>
				<hr>
				<input type="file" name="file">
				<div class="row">
					<div class="col-sm-2">
					</div>
					<div class="col-sm-4 text-center">
							<button type="submit" class="btn btn-primary center-block" style="font-size: 14px;" form="update">&nbsp;&nbsp;Update&nbsp;&nbsp;</button>
					</div>
					<div class="col-sm-4 text-center">
							<a type="button" class="btn btn-secondary center-block" style="font-size: 14px;" href="profile.php">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</a>
					</div>
					<div class="col-sm-2">
					</div>
				</div>
			</form>
		</div>
	</body>
</html>