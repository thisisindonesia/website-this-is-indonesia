<?php
error_reporting(0);
session_start();
$email = $_SESSION['email'];
$conn = mysqli_connect('localhost','root','','thisisindonesia');
$coba = mysqli_query('select * from user');
if ($coba !== true) {
$conn = mysqli_connect('192.168.1.2:3306','root','pass','thisisindonesia');
}
$query = "select concat(a.nama_depan,' ',a.nama_belakang) as lengkap, a.email, a.foto_profile, b.image as gambar, b.caption, b.id as idPost ,b.tanggal from user a join post b on a.email = b.email where a.email='$email'";
$result = mysqli_query($conn, $query);
$queryFoto = "select foto_profile FROM user WHERE email = '$email'";
$resultFoto = mysqli_query($conn,$queryFoto);
$photo = mysqli_fetch_array($resultFoto);
$foto = $photo[0];
?>
<!DOCTYPE html>
<html>
<head>
<title>My Post</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
  </head>
  <body>
  <nav class="navbar navbar-expand-lg bg-light navbar-light">
    <a class="navbar-brand" href="">
    <img src="logo.png" alt="Logo" style="width: 100px;">
    </a>
    <ul class="nav navbar-nav ml-auto">
		<li class="nav-item">
    <a class="nav-link" href="../Abdullah/home.php">HOME</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="../Fatur/OurCulture.php">OUR CULTURE</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="../Andika/Contact.php">ABOUT US</a>
    </li>
    <li class="nav-item">
    <a href="../Fatur/PostUtama.php"><button type="button" class="btn btn-primary">+ Upload</button></a>
		</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
					<img src="../Abdullah/upload/<?=$foto?>" class="rounded-circle" style="width: 40px; height: 40px;">
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="../Fikri/c.php">My Post</a>
					<a class="dropdown-item" href="../Fatur/profile.php">My Profile</a>
					<a class="dropdown-item" href="../Abdullah/controller/logout.php">Log out</a>					
        </div>
      </li>
      </ul>
		</nav>
  <div class="container-fluid">
    <div class="thumbnail text-center">
      <img src="qq.jpg" alt="logo"  style="width: 100%; ">
        <div class="carousel-caption">
          <p style="font-size: 40px; color: white; margin-bottom: 10%"> </p>
        </div>
    </div>
  </div>
  <br>
  <div class="container-fluid">
  <h1>My Post</h1>
  </div>
  <br>
  <hr style="width: 100%;">
  <?php
    if(mysqli_num_rows($result) > 0){
      while($baris = mysqli_fetch_array($result)){
      echo 
      '
      <div class="container-fluid">
      <div class="card" style="width: 100%">
        <div class="row no-gutters"">
          <div class="col-md-8">
            <img src="../Abdullah/images/'.$baris["gambar"].'" class="card-img" alt="...">
          </div>
          <div class="col-md-4">
        <div class="card-body">
        <div style="opacity:0.5; text-align: right;"><small>'.$baris["tanggal"].'</small></div>
          <div class="row"> 
           <div class="col-sm-2">
            <img  style ="width:50px; height:50px" class="rounded-circle" src="../Abdullah/upload/'.$baris["foto_profile"].'">
           </div>
         <div class="col-sm-10">
          <b>'.$baris["lengkap"].'</b>
            <div style="opacity:0.5"><p><small>'.$baris["email"].'</small></p></div>
          </div>
        <form id="post" method="POST"> 
        <div class="container shadow p-3 mb-5 bg-white rounded" style="width:100%">
        <textarea class="form-control rows="4" style="border: none; height: 120px" name="caption">'.$baris["caption"].'</textarea>
        </div>
        </form>
        <div class="container" style="text-align:center">
          <button type="submit" name="update" value="'.$baris['idPost'].'" class="btn btn-primary center-block" style="font-size: 20px;" form="post" formaction="../Fahmi/update.php">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          <button type="submit" name="delete" value="'.$baris['idPost'].'" class="btn btn-outline-primary center-block" style="font-size: 20px;" form="post" formaction="delete.php">&nbsp;&nbsp;Delete&nbsp;&nbsp;</button>
        </div>
        </div>
      </div>
       </div>
        </div>
        </div>
      </div>
      <br>
      <br>
      ';  
      }
    } 
  ?>
  </body>
</html>