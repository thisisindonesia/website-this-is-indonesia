-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Nov 2019 pada 04.28
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thisisindonesia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `destination`
--

CREATE TABLE `destination` (
  `id` int(10) NOT NULL,
  `gambar` text NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `destination`
--

INSERT INTO `destination` (`id`, `gambar`, `nama`, `deskripsi`) VALUES
(1, 'https://cdn.discordapp.com/attachments/625308633005293599/646250563729621003/Tanah-Lot-Tabanan-Bali.jpg', 'Pura Tanah Lot', 'Attraction, unique view of the temple in the middle of the sea,\r\nsunset views and supporting facilities for …...');

-- --------------------------------------------------------

--
-- Struktur dari tabel `halaman`
--

CREATE TABLE `halaman` (
  `id` int(11) NOT NULL,
  `nama_tempat` text NOT NULL,
  `intro` text NOT NULL,
  `get_around` text NOT NULL,
  `get_there` text NOT NULL,
  `foto` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `halaman`
--

INSERT INTO `halaman` (`id`, `nama_tempat`, `intro`, `get_around`, `get_there`, `foto`) VALUES
(1, 'Bromo Tengger Semeru National Park', 'The Bromo Tengger Semeru National Park covers a massive area of 800 square km in the center of East Java. For anyone with an interest in volcanoes, a visit to the park is a must. This is the largest volcanic region in the province. Visit the park and see the plumes of smoke coming from Mt Semeru, an active volcano which rises 3,676 meters above sea level. Experience the remarkable Tengger Caldera, Java\'s largest, with its 10 km barren desert-like sea of sand. Within the caldera rise the deeply fissured volcanic cones of Batok and Bromo, the latter is still active with a cavernous crater from which smoke blows skyward. Temperatures at the top of Mt Bromo range about 5-18 degrees Celsius. To the south of the park is a rolling upland plateau dissected by valleys and dotted with several small scenic lakes, extending to the foot of Mt Semeru.', 'Many visitors to the park choose to walk while they are here. There are clearly marked tracks across the sand sea that leads to the foot of Bromo. Alternatively, you can rent a jeep or hire a horse from Cemoro Lawang to get around. \r\n', 'Bromo Tengger Semeru can be reached by private and public vehicle from Surabaya or Malang in East Java. Sriwijaya Air flies twice daily to Malang from Jakarta.  There are multiple ways to get into the park. Visitors can come from Probolinggo, in the north west arrive through the village of Ngadisari. Or take the north east approach via Pasuruan and the village of Tosari. The third, the more difficult approach is via Ngadas, which is best traveled on the way down. The Probolinggo approach is the easiest and by far the most popular route, especially if travelling by public bus. Wonokitri is the closest and the easiest approach if you are coming by private vehicle from Surabaya (5 hours journey). To get closer to Mt Bromo you must rent 4x4 vehicles (there are many 4x4 vehicles available for rent there). Most tour groups from Surabaya stay overnight at Tretes, where there are a number hotels, as there are in Malang, which has the added advantage of having an airport.  Alternatively, you can contact a travel agency to arrange your trip.\r\n', 'https://cdn.discordapp.com/attachments/499592727034331149/643713384017166336/scape.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `caption` text NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `post`
--

INSERT INTO `post` (`id`, `email`, `image`, `caption`, `tempat`, `kota`, `tanggal`) VALUES
(1, 'akucantik@gmail.com', 'post1.jpg', 'Gile Keren banget, kek benar-benar pemandangan terindah yang pernah gua liat di muka bumi ini. Pantes banget sih ini dikunjungi', 'Jalur Ambyar', 'Balikpapan', '2019-11-12'),
(4, 'abdullah12@gmail.com', 'titanium.PNG', 'Hello www', 'Jawa', 'Jatim', '2019-11-26'),
(5, 'abdullah12@gmail.com', 'maxresdefau1lt.jpg', 'Enter Caption', 'Jakarta', 'Jakarta', '2019-11-26'),
(6, 'abdullah12@gmail.com', 'L7TWoh6NF6U.jpg', 'ANJAY INI MAH< ::::::::)))))))))))))', 'Dermaga Bundar sari', 'Bandung', '2019-11-26'),
(10, 'fatur@gmail.com', 'header jkt.jpg', 'OWADAS', 'ba', 'bandung', '2019-11-26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `foto_profile` text DEFAULT NULL,
  `tentang_saya` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `email`, `nama_depan`, `nama_belakang`, `password`, `foto_profile`, `tentang_saya`) VALUES
(1, 'andika@gmail.com', '12345', '54321', '12345', 'gambar1.png', 'Bismillah'),
(2, 'akucantik@gmail.com', 'kamu', 'jelek', 'iyasama', 'alhaqq.png', 'Saya pecinta alam'),
(3, 'abdullah12@gmail.com', 'alex', 'abdul', '12345', 'B4pnRyxCUAEBEXY.jpg', 'Sayang ayah'),
(4, 'fatur@gmail.com', 'Fatur', 'rahman', '12345 ', 'gambar1.png', 'Bismillah');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_email_post` (`email`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `destination`
--
ALTER TABLE `destination`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_email_post` FOREIGN KEY (`email`) REFERENCES `user` (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
