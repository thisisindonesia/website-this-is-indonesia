<?php
	include('config/config.php');
	session_start();

	$email = $_SESSION['email'];
	$query = mysqli_query($connect,"SELECT foto_profile FROM user WHERE email = '{$email}'");
	$hasil = mysqli_fetch_array($query);
	$avatar = $hasil[0];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Welcome to This Is Indonesia</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body style="background-color: #f4f4f4">
	<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-expand-sm bg-light navbar-light fixed-top">
				<div class="col-md-7">
					<a class="navbar-brand" href="home.php">
						<img src="images/logo.png" alt="Logo" style="width:95px;">
					</a>
				</div>
				<div class="col-md-5">
					<ul class="navbar-nav">
						<li class="nav-item active" style="margin-right: 5px"><a class="nav-link" href="home.php">HOME</a></li>
						<li class="nav-item" style="margin-right: 5px"><a class="nav-link" href="../Fatur/ourCulture.php">OUR CULTURE</a></li>
						<li class="nav-item" style="margin-right: 70px"><a class="nav-link" href="../Andika/Contact.php">ABOUT US</a></li>
						<li class="nav-item" style="margin-right: 15px">
						<button class="btn btn-primary" type="" name="" data-toggle="modal" data-target="#modalUpload">UPLOAD</li>
						<li class="nav-item dropdown">
					      	<img src="upload/<?= $avatar ?>" class="dropdown-toggle rounded-circle" id="navbardrop" data-toggle="dropdown" width="40" height="40">
					      <div class="dropdown-menu">
						  	<a class="dropdown-item" href="../Fikri/c.php">My Post</a>
					    	<a class="dropdown-item" href="../Fatur/profile.php">Profile</a>
					        <a class="dropdown-item" href="controller/logout.php">Logout</a>
					      </div>
					    </li>
					</ul>
				</div>
			</nav>
		</div>
	</div>

	<div class="modal" id="modalUpload">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
          			<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form method="POST" action="controller/addpost.php" enctype='multipart/form-data'>
						<div class="form-group" style="height: 200px; padding: 80px 130px; border: 1px dashed gray; border-radius: 5px">
							<input type="file" class="form-control" name="file" required></input>
						</div>
						<div class="form-group">
							<label for="place">Place :</label>
							<input type="place" class="form-control" name="place" placeholder="Enter Place" required></input>
						</div>
						<div class="form-group">
							<label for="city">City :</label>
							<input type="text" class="form-control" name="city" placeholder="Enter City" required></input>
						</div>
						<div class="form-group">
							<label for="caption">Caption :</label>
							<input type="text" class="form-control" name="caption" placeholder="Enter Caption" required></input>
						</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="submit" name="submit" value="submit" style="width: 100px">Post</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div id="myShow" class="carousel slide" data-ride="carousel">
		<ul class="carousel-indicators">
			<li data-target="#myShow" data-slide-to="0" class="active"></li>
			<li data-target="#myShow" data-slide-to="1"></li>
			<li data-target="#myShow" data-slide-to="2"></li>
		</ul>

		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="images/candi.jpg" class="img-fluid">
				<div class="carousel-caption">
   					<h3>Borobudur Temple</h3>
    				<p>Jl. Badrawati, Kw. Candi Borobudur, Borobudur, Kec. Borobudur, <br>Magelang, Jawa Tengah</p>
  				</div>
  				<div class="carousel-caption">
   					<h1><a href="../Fikri/a.php" style="color: white">Welcome to<br> This is Indonesia</a></h1> 
  				</div>
			</div>

			<div class="carousel-item">
				<img src="images/bali.jpg" class="img-fluid">
				<div class="carousel-caption">
   					<h3>Ulun Danu Beratan Temple</h3>
    				<p>Danau Beratan, Candikuning, Baturiti, <br>Tabanan Regency, Bali</p>
  				</div>
  				<div class="carousel-caption">
   					<h1><a href="../Fikri/a.php" style="color: white">Welcome to<br> This is Indonesia</a></h1> 
  				</div>
			</div>

			<div class="carousel-item">
				<img src="images/rajaampat.jpg" class="img-fluid">
				<div class="carousel-caption">
   					<h3>Raja Ampat Islands</h3>
    				<p>West Papua</p>
  				</div>
  				<div class="carousel-caption">
   					<h1><a href="../Fikri/a.php" style="color: white">Welcome to<br> This is Indonesia</a></h1> 
  				</div>
			</div>
		</div>

		<a class="carousel-control-prev" href="#myShow" data-slide="prev">
    		<span class="carousel-control-prev-icon"></span>
  		</a>
  		<a class="carousel-control-next" href="#myShow" data-slide="next">
    		<span class="carousel-control-next-icon"></span>
  		</a>
	</div>

	<div>
		<img src="images/gate.jpg" class="img-fluid">
		<a href="../Andika/destinationJava.php"><button type="button" class="btn btn-light" style=" left: 30%;">What to see</button></a>
		<a href="../Fahmi/tripIdeas.php"><button type="button" class="btn btn-light" style=" left: 50%;">What to do</button></a>
		<a href="../Fatur/PostUtama.php"><button type="button" class="btn btn-light" style=" left: 70%;">Share your trip</button></a>
	</div>

	<div class="divHighlights" style="width:100%; padding-left: 16px;">
		<h2 style="margin-top: 60px">Destination Highlights</h2>
		<img src="images/medan.jpg" class="img-thumbnail" alt="" width="325" height="500">
		<div class="text-block" style="left: 13%;">
			<h4>Medan</h4>
		</div>

		<img src="images/bromo.jpg" class="img-thumbnail" alt="" width="325" height="400">
		<div class="text-block" style="left: 38%;">
			<h4>Jawa Tengah</h4>
		</div>

		<img src="images/semarang.jpg" class="img-thumbnail" alt="" width="325" height="400">
		<div class="text-block" style="left: 62%;">
			<h4>Semarang</h4>
		</div>

		<img src="images/surabaya.jpg" class="img-thumbnail" alt="" width="325" height="400">
		<div class="text-block" style="left: 86%;">
			<h4>Surabaya</h4>
		</div>
	</div>

	<!--https://mdbootstrap.com/docs/jquery/navigation/footer/-->
	<footer style="background-color: white; margin-top: 60px">
  		<div class="footer-copyright text-center py-3">© 2019 Copyright</div>
	</footer>
</body>
</html>