<?php
error_reporting(0);
session_start();
$email = $_SESSION['email'];
$conn = mysqli_connect('localhost','root','','thisisindonesia');
$coba = mysqli_query('select * from user');
if ($coba !== true) {
$conn = mysqli_connect('192.168.1.2:3306','root','pass','thisisindonesia');
}
$query = "select foto_profile FROM user WHERE email = '$email'";
$hasil = mysqli_query($conn,$query);
$ambil = mysqli_fetch_array($hasil);
$foto = $ambil[0];
?>
<html lang="en">
    <head>
        <title> Navigation Bar Language </title>
        <meta charset =utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-light navbar-light">
    <div class ="row">
    <div class="col-md-8"></div>
    </div>
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">
    <img src="thisindo.png" alt="logo" style="width:25%;">
  </a>
  
  <!-- Links -->
      <div class="col-md-7"></div>
  </div>
  <ul class="navbar-nav">
    <li class="nav-item">
    <a class="nav-link" href="../Abdullah/home.php">HOME</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../Fatur/OurCulture.php">OUR CULTURE</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../Andika/Contact.php">ABOUT US</a>
      </li>
      <li class="nav-item">
     <a href="../Fatur/PostUtama.php"><button type="button" class="btn btn-primary">+ Upload</button></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          <img src="../Abdullah/upload/<?=$foto?>" class="rounded-circle" style="width: 40px; height: 40px;">
        </a>
        <div class="dropdown-menu">
        <a class="dropdown-item" href="../Fatur/PostUtama.php">My Post</a>
          <a class="dropdown-item" href="../Fatur/profile.php">My Profile</a>
          <a class="dropdown-item" href="../Abdullah/controller/logout.php">Log out</a>   
        </div>
      </ul>
    </nav>

<div class="img-container">
  <img src="kereta.jpg" style="width: 100%">
<div class="overlay">
<div class="carousel-caption">
    <p style="font-size: 50px; color:white; margin-bottom: 20%"> <b> L A N G U A G E </b> </p>
</div>
</div>
</div>
<br>

<div class="container-fluid">
<a class="" href="#"><p style="color: black"> Back </p></a> 
<hr>
<div class="container-fluid">
    <p>
            Bahasa Indonesia is the national and official language of Indonesia and is 
            used in the entire country. It is the language of official communication, taught
            in schools and used for broadcast in electronic and digital media. 
            Most Indonesians also have their own ethnic language and dialect, with the
            most widely spoken being Javanese and Sundanese. Some ethnic Chinese
            communities continue to speak various Chinese 
            dialects, most notably Hokkien in Medan and Teochew in Pontianak.
            <br>
            While generally is not widely spoken, an acceptable level of English can be
            understood in a number of major cities and tourists’ destinations including Bali, 
            Batam, Jakarta, Bandung, Surabaya, and Yogyakarta. Moreover, most hotel and 
            airlines staff can also communicate in English on a basic to moderate level.
            </p>
            </div>      
            <br>
    <p><strong> Common Phrases </strong></p>

<div class="container-fluid">
    <p style="float-left; color: black;">
            Indonesian pronunciation is relatively easy to master. Each letter always
            represents the same sound and most letters are pronounced the same as their
            English counterparts. The following are examples of some of the common.
        </p>
        </div>
<br>
<div class="container-fluid">
    <p>            
        Phrases in Bahasa Indonesia that can be useful in conversation:
            <br>
            Good Morning = Selamat Pagi<br>
            Good Day = Selamat Siang<br>
            Good Afternoon = Selamat Sore<br>
            Good Evening/Good Night  = Selamat Malam<br>
            Goodbye  = Selamat Tinggal<br>
            How are you? =  Apa Kabar<br>
            I’m Fine/Good/Great  = Baik-baik (as an answer to Apa Kabar)<br>
            Excuse me = Permisi<br>
            Sorry = Maaf<br>
            Please = Silahkan<br>
            Help! = Tolong!<br>
            Thank You = Terima Kasih<br>
            You’re Welcome = Terima kasih Kembali<br>
            Yes = Ya<br>
            Tidak = No<br>
            Mr/Sir = Bapak<br>
            Ms/Ms/Madam = Ibu<br>
            Miss = Nona<br>
            How much/many = Berapa<br>
            How much is it (price)? = Berapa Harganya?<br>
            Where is = Dimana <br>
            How to get There = bagaimana caranya kesana?<br>
            I want = Saya Mau <br>
            I don’t want = Saya Tidak Mau <br>
            </p>
            </div>      
</body>
</html>

    </head>
</html>

<!---Sumber pengerjaan projek saya dari website https://w3schools.com-->